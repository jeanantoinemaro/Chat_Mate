# Groupe de martin_p 974594

## Projet "Chat_Mate"

## Par l'équipe "Chat_Team"

- Léna MARTIN
- Jean-Antoine MARO
- Ali DANESH
- Pierre CHARDAT

### But du projet

Réaliser une application de type IRC ("Internet Relay Chat") géré et utilisée depuis le terminal.

### Organisation

To-do list (Board and issues on our repo GitLab) : https://rendu-git.etna-alternance.net/module-8870/activity-49211/group-974594

Notre project schedule network diagram : https://drive.google.com/file/d/1FmSs5JKG_0AhEo9YhcpMp0PRH5FbEAYd/view?usp=sharing

Notre UML (modèle relationnel, méthode MERISE) : https://www.figma.com/file/wZ4jo9HrF8jqRXUiUmREDS/Untitled?node-id=1%3A2836
